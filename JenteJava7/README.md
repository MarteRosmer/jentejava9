#JenteJava Vår 2016

Oppgaven for i dag er å lage ulike typer mennesker ved hjelp av arv. Hver type mennesker skal ha de minimale med egenskaper som de må oppfylle. Dette er navn, kjønn, alder, muligheten til å hente informasjon om dem, muligheten til å endre informasjon om dem og muligheten til å skrive ut informasjon om dem.

Det skal finnes følgende typer av mennesker; programmerere, studenter, professorer og jentejava-medlem. Alle studenter og professorer er programmerere, men ikke alle programmerere er studenter eller professorer. Et jentejava-medlem er en student og er derfor også en programmerer.

En programmerer har en liste som sier hvilke programmeringsspråk de kan. De kan også lære seg nye språk som de kan legge til i denne listen. Ettersom en programmerer gjerne vil dele hvor mye de kan, skal det være mulig å skrive ut alle språkene de kan.

En student har tre fag de er meldt på. Det er mulig å endre hvilke fag de har, men ikke endre antallet av fagene. De har også en variabel som sier hvor ofte de er på universitetet.

En professor har tre fag de foreleser i. Det er mulig å endre hvilke fag de har, men ikke endre antallet av fagene. De har også en variabel som sier hvor ofte de er på universitetet.  

Et jentejava-medlem har en variabel som sier om de er på jentejava-forelesning eller ikke. 
