package test;

import static org.junit.Assert.*;

import org.junit.*;

import main.JenteJava;
import main.Kjønn;
import main.Programmerer;
import main.Student;

public class systemTest {

	
	JenteJava jenteJava; 
	
	@Before
	public void setup(){
		jenteJava = new JenteJava("Karina",Kjønn.JENTE,22);
	}
	@Test
	public void enJenteBørKunneJava() {
		jenteJava.hentNavn();
		jenteJava.leggTilSpråk("Java");
		assertEquals("[Java]",jenteJava.skrivUtSpråk());
	}
	
	@Test
	public void enUtviklerBørKunneJavaScriptOgJava() {
		Programmerer jente = new Programmerer("Siri",Kjønn.JENTE,23);
		jente.leggTilSpråk("JavaScript");
		jente.leggTilSpråk("Java");
		assertEquals("[JavaScript, Java]",jente.skrivUtSpråk());
	}

	@Test
	public void enJenteBørBruke10TimerPåUiB() {
		jenteJava.leggTilNyeTimer(10);
		assertEquals(10.0,jenteJava.hentAntallTimerPåUiB(),0.0);
	}

	@Test
	public void skalKunneSkriveUtGenerellInfoOmStudent() {
		Student gutt = new Student("Daniel",Kjønn.GUTT,20);
		assertEquals("Navn: Daniel\n Kjønn: GUTT\n Alder: 20",gutt.toString());
	}
	
	@Test
	public void skalKunneSkriveUtGenerellInfoOmDeltagerPåJenteJava() {
		assertEquals("Navn: Karina\n Kjønn: JENTE\n Alder: 22",jenteJava.toString());
	}
	

	// Her kan dere skrive flere tester
}
