package main;

import java.util.ArrayList;

public class Programmerer extends Menneske{

	ArrayList<String> språkListe = new ArrayList<>();
	
	public Programmerer(String navn, Kjønn kjønn, int alder) {
		super(navn, kjønn, alder);
	}
	
	public void leggTilSpråk(String språk){
		språkListe.add(språk);
	}
	
	public String skrivUtSpråk(){
		return språkListe.toString();
	}

}
