package main;

import java.util.ArrayList;

public class UIBBruker extends Programmerer {
	private ArrayList<String> fagListe = new ArrayList<>(3);
	private double antallTimerPåUiB = 0;
	
	public UIBBruker(String navn, Kjønn kjønn, int alder) {
		super(navn, kjønn, alder);	
	}
	
	protected void leggTilFag(String fag){
		fagListe.add(fag);
	}
	
	protected String skrivUtFag(){
		return fagListe.toString();
	}

	public double hentAntallTimerPåUiB() {
		return antallTimerPåUiB;
	}

	protected void settAntallTimerPåUiB(double antallTimerPåUiB) {
		this.antallTimerPåUiB = antallTimerPåUiB;
	}
	
	public void leggTilNyeTimer(int antallNyeTimer){
		this.antallTimerPåUiB += antallNyeTimer;
	}
	
}
