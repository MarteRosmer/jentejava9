package main;

public class Menneske {
	
	private String navn;
	private Kjønn kjønn;
	private int alder;
	
	public Menneske(String navn, Kjønn kjønn, int alder){
		this.settNavn(navn);
		this.settKjønn(kjønn);
		this.settAlder(alder);
	}

	public String hentNavn() {
		return navn;
	}

	public void settNavn(String navn) {
		this.navn = navn;
	}

	public Kjønn hentKjønn() {
		return kjønn;
	}

	public void settKjønn(Kjønn kjønn) {
		this.kjønn = kjønn;
	}

	public int hentAlder() {
		return alder;
	}

	public void settAlder(int alder) {
		this.alder = alder;
	}

	@Override
	public String toString() {
		return "Navn: " + navn + "\n " + "Kjønn: " + kjønn + "\n " 
				+ "Alder: " + alder;
	}
	
	
}
