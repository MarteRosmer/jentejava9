package main;

public class JenteJava extends Student {
	private boolean erPåJenteJava;

	public JenteJava(String navn, Kjønn kjønn, int alder) {
		super(navn, kjønn, alder);
	}

	public boolean erPåJenteJava() {
		return erPåJenteJava;
	}

	public void draPåJenteJava(boolean erPåJenteJava) {
		if(erPåJenteJava){
			System.out.println("Du er jo allerede på JenteJava!");
		}else{
			this.erPåJenteJava = erPåJenteJava;
		}
	}

	
}
